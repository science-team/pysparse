pysparse (1.1.1-2) unstable; urgency=medium

  * Team upload.  
  * [b8b40f0] Update vcs-urls (migrate to salsa)
  * [4d76f58] Update Standards-Version to 4.1.4
  * [66b750e] Set compat level 11 (Closes: #539975)
  * [c4bd4e7] Remove myself from uploaders

 -- Anton Gladky <gladk@debian.org>  Sat, 19 May 2018 14:07:03 +0200

pysparse (1.1.1-1) unstable; urgency=medium

  * Team upload.  
  * New upstream version 1.1.1
  * Update patch queue.
    - Drop no-g2c.patch, applied upstream.
    - Refresh suitesparse-4.1.patch.
    - Drop superlu-4.3.patch, no longer applicable.
  * Add patch fixing FTBFS with SuperLU v5.x.
    Thanks to Drew Parsons for reporting (Closes: #836677)
  * Use pybuild.
  * Use dh-python.

 -- Ghislain Antony Vaillant <ghisvail@gmail.com>  Fri, 23 Sep 2016 14:45:44 +0100

pysparse (1.1-2) unstable; urgency=medium

  * [b3b4581] Remove outdated files.
  * [8ea3049] Use current compat level and source-format.
  * [8c57901] Add misc:Depends to dependencies.
  * [c664014] Clean d/rules. (Closes: #822018)
  * [e6ab663] Fix warnings in doc-base.
  * [5941bf8] Add docs, examples and install files.
  * [c56894f] Move package under the Debian Science team.
  * [346f661] Apply cme fix dpkg-control.

 -- Anton Gladky <gladk@debian.org>  Sat, 03 Sep 2016 23:34:57 +0200

pysparse (1.1-1.3) unstable; urgency=medium

  * Non-maintainer upload.
  * Transition to superlu 4.3:
    - debian/control: replace libsuperlu3-dev by libsuperlu-dev.
    - superlu-4.3.patch: new patch, fixes FTBFS against superlu 4.3
      (Closes: #740636).
  * Use new names of virtual -dev packages for BLAS and LAPACK.

 -- Sébastien Villemot <sebastien@debian.org>  Mon, 10 Mar 2014 17:49:01 +0100

pysparse (1.1-1.2) unstable; urgency=low

  * Non-maintainer upload.
  * suitesparse-4.1.patch: new patch, fixes FTBFS against suitesparse 4.1
    (Closes: #708379)

 -- Sébastien Villemot <sebastien@debian.org>  Mon, 05 Aug 2013 12:04:49 +0000

pysparse (1.1-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Covnert to dh_python2 (Closes: #551697, #616986).

 -- Andrea Colangelo <warp10@ubuntu.com>  Fri, 28 Jun 2013 16:51:51 +0200

pysparse (1.1-1) unstable; urgency=low

  * New upstream release.
  * Small changes to package description.
  * Bounds-checking eliminates many crashes (closes: #535318).
  * Added --install-layout=deb to setup.py install (closes: #547839).
  * Using quilt for patches, and added README.source describing it.
  * Updated Standards-Version.

 -- Adam C. Powell, IV <hazelsct@debian.org>  Tue, 29 Sep 2009 19:28:55 -0400

pysparse (1.0.1-6) unstable; urgency=low

  * Added python-numpy to python-sparse Depends (closes: #539694).
  * Updated watch file (closes: #450374).
  * Updated Standards-Version.

 -- Adam C. Powell, IV <hazelsct@debian.org>  Tue, 04 Aug 2009 09:51:35 -0400

pysparse (1.0.1-5.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix pysparse.superlu module port to superlu 3 based on reading
    the docs. Closes: #526527

 -- Thomas Viehmann <tv@beamnet.de>  Fri, 01 May 2009 20:56:39 +0200

pysparse (1.0.1-5) unstable; urgency=low

  * Removed removal of /usr/lib and Build-Depends on python-central >= 0.6
    (closes: #472026).

 -- Adam C. Powell, IV <hazelsct@debian.org>  Mon, 24 Mar 2008 16:29:44 -0400

pysparse (1.0.1-4) unstable; urgency=low

  * New upload for unstable.
  * Removed a few unnecessary library linkages.

 -- Adam C. Powell, IV <hazelsct@debian.org>  Fri, 22 Feb 2008 10:39:25 -0500

pysparse (1.0.1-3) experimental; urgency=low

  * Rebuild against, and depend on, suitesparse with proper shared libs.

 -- Adam C. Powell, IV <hazelsct@debian.org>  Mon, 11 Feb 2008 18:28:48 -0500

pysparse (1.0.1-2) experimental; urgency=low

  * Changed build-depends to gfortran and its BLAS and LAPACK libs
    (closes: #464973).
  * Added build-depends on suitesparse and superlu, and changed build system to
    use installed umfpack and superlu instead of included source.
  * Doing this required forward-porting to superlu 3.0.

 -- Adam C. Powell, IV <hazelsct@debian.org>  Sun, 10 Feb 2008 07:56:54 -0500

pysparse (1.0.1-1) unstable; urgency=low

  * New upstream release.

 -- Adam C. Powell, IV <hazelsct@debian.org>  Wed, 20 Jun 2007 07:52:22 -0400

pysparse (1.0-1) unstable; urgency=low

  * New upstream release (closes: #419549).
  * Adds the amd directory.

 -- Adam C. Powell, IV <hazelsct@debian.org>  Mon, 23 Apr 2007 18:10:11 -0400

pysparse (0.34.032-2) unstable; urgency=low

  * Updated copyright gives authors and licenses for files in superlu and
    umfpack directories.

 -- Adam C. Powell, IV <hazelsct@debian.org>  Fri, 19 Jan 2007 17:56:30 -0500

pysparse (0.34.032-1) unstable; urgency=low

  * Initial release based on Mathias Klose' python-numeric package (closes:
    #399505).
  * Linking with -lblas-3 and -llapack-3.

 -- Adam C. Powell, IV <hazelsct@debian.org>  Fri, 24 Nov 2006 00:15:15 -0500
